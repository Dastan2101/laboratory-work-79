const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const accounting = require('./app/accounting');
const categories = require('./app/categories');
const location = require('./app/location');

const app = express();
app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : '1qaz@WSX29',
    database: 'inventory'
});


app.use('/accounting', accounting(connection));
app.use('/categories', categories(connection));
app.use('/locations', location(connection));

connection.connect((err) => {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);

    app.listen(port);
});

