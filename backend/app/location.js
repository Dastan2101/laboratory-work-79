const express = require('express');
const router = express.Router();


const createRouter = connection => {

    router.get('/', (req, res) => {

        connection.query('SELECT * FROM `location`', (error, result) => {
            if (error) {
                res.status(500).send({error: "Location error"})
            }
            const filter = [];

            result.map(item => {
                filter.push({id: item.id, title: item.title})
            });

            res.send(filter)

        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `location` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({error: "Location id not found"})
            }
            res.send(result[0]);

        });
    });

    router.post('/', (req, res) => {

        const location = req.body;

        connection.query('INSERT INTO `location` (`title`, `description`) VALUES (?, ?)',
            [location.title, location.description],
            (error, result) => {
                if (error) {
                    res.status(500).send({error: "Error"})
                }
                location.id = result.insertId;
                res.send(location)
            });

    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `location` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({message: error.sqlMessage})
            }

            res.send({message: "Success"})
        })
    });

    return router
};


module.exports = createRouter;