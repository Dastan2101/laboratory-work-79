const express = require('express');
const router = express.Router();


const createRouter = connection => {

    router.get('/', (req, res) => {

        connection.query('SELECT * FROM `categories`', (error, result) => {
            if (error) {
                res.status(500).send({error: "Categories error"})
            }
            const filter = [];

            result.map(item => {
                filter.push({id: item.id, title: item.title})
            });

            res.send(filter)

        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `categories` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({error: "Category id is error"})
            }
            res.send(result[0]);

        });
    });

    router.post('/', (req, res) => {

        const category = req.body;

        connection.query('INSERT INTO `categories` (`title`, `description`) VALUES (?, ?)',
            [category.title, category.description],
            (error, result) => {
                if (error) {
                    res.status(500).send({error: "Error"})
                }
                category.id = result.insertId;

                res.send(category)
            });

    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `categories` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({message: error.sqlMessage})
            }

            res.send({message: "Success"})
        })
    });

    return router
};


module.exports = createRouter;