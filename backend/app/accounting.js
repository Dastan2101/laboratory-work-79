const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {

    router.get('/', (req, res) => {

        connection.query('SELECT * FROM `accounting`', (error, result) => {
            if (error) {
                res.status(500).send({error: "Database error"})
            }

            const filter = [];

             result.map(item => {
                filter.push({id: item.id, title: item.title, category_id: item.category_id, location_id: item.location_id})
            });

            res.send(filter)

        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `accounting` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({error: "Database error"})
            }
            res.send(result[0]);

        });
    });

    router.post('/', upload.single('image'), (req, res) => {

        const inventory = req.body;

        if (req.file) {
            inventory.image = req.file.filename;
        }

        const reqBody = req.body;

            connection.query('INSERT INTO `accounting` (`category_id`, `location_id`, `title`, `description`, `image`) VALUES (?, ?, ?, ?, ?)',
                [inventory.category_id, inventory.location_id, inventory.title, inventory.description, inventory.image],
                (error, result) => {
                    if (error) {
                        res.status(500).send({error: "Couldn't find"})
                    }
                    reqBody.id = result.insertId;
                    res.send(reqBody)
                });

    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `accounting` WHERE `id` = ?', req.params.id, (error, result) => {
            if (error) {
                res.status(500).send({message: error.sqlMessage})
            }

            res.send({message: "Success"})
        })
    });


    return router
};


module.exports = createRouter;