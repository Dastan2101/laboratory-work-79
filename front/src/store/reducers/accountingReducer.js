import {
    FETCH_CATEGORIES_SUCCESS, FETCH_CATEGORY_SUCCESS,
    FETCH_INVENTORIZATION_LIST,
    FETCH_LOCATION_SUCCESS,
    FETCH_LOCATIONS_SUCCESS,
    FETCH_MORE_INFO_SUCCESS
} from "../actions/actions";

const initialState = {
    inventList: [],
    fullInfo: {},
    locations: [],
    locationInfo: {},
    categories: [],
    category: {},
};

const accountingReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_INVENTORIZATION_LIST:
            return {...state, inventList: [...state.inventList].concat(action.data)};

        case FETCH_MORE_INFO_SUCCESS:
            return {...state, fullInfo: action.data};
        case FETCH_LOCATIONS_SUCCESS:
            return {...state, locations: [...state.locations].concat(action.data)};
        case FETCH_LOCATION_SUCCESS:
            return {...state, locationInfo: action.location};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: [...state.categories].concat(action.data)};
        case FETCH_CATEGORY_SUCCESS:
            return {...state, category: action.category};
        default:
            return state;
    }
};

export default accountingReducer;