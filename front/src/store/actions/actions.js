import axios from '../../axios-inventory';

export const FETCH_INVENTORIZATION_LIST = 'FETCH_INVENTORIZATION_LIST_SUCCESS';
export const FETCH_MORE_INFO_SUCCESS = 'FETCH_MORE_INFO_SUCCESS';
export const FETCH_LOCATIONS_SUCCESS = 'FETCH_LOCATIONS_SUCCESS';
export const FETCH_LOCATION_SUCCESS = 'FETCH_LOCATION_SUCCESS';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';

export const fetchInventList = data => ({type: FETCH_INVENTORIZATION_LIST, data});
export const fetchMoreInfo = data => ({type: FETCH_MORE_INFO_SUCCESS, data});
export const fetchLocations = data => ({type: FETCH_LOCATIONS_SUCCESS, data});
export const fetchLocation = location => ({type: FETCH_LOCATION_SUCCESS, location});
export const fetchCategories = data => ({type: FETCH_CATEGORIES_SUCCESS, data});
export const fetchCategory = category => ({type: FETCH_CATEGORY_SUCCESS, category});


export const getInventList = () => {
    return dispatch => {
        return axios.get('/accounting').then(response => {
            dispatch(fetchInventList(response.data))
        })
    }
};

export const getFullInfo = (id) => {
    return dispatch => {
        return axios.get('/accounting/' + id).then(response => {
            dispatch(fetchMoreInfo(response.data))
        })
    }
};

export const getLocations = () => {
    return dispatch => {
        return axios.get('/locations').then(response => {
            dispatch(fetchLocations(response.data))
        })
    }
};

export const getLocation = (id) => {
    return dispatch => {
        return axios.get('/locations/' + id).then(response => {
            dispatch(fetchLocation(response.data))
        })
    }
};

export const getCategories = () => {
    return dispatch => {
        return axios.get('/categories').then(response => {
            dispatch(fetchCategories(response.data))
        })
    }
};

export const getCategory = id => {
    return dispatch => {
        return axios.get('/categories/' + id).then(response => {
            dispatch(fetchCategory(response.data))
        })
    }
};

export const createAccItem = item => {
    return dispatch => {
        return axios.post('/accounting', item).then(response => {
            dispatch(fetchInventList(response.data))
            getCategories();
            getLocations();
        })
    }
};

export const inventItem = id => {
        return axios.delete('/accounting/' + id).then(
            window.location.reload()
    )
};

export const createCategory = item => {
    return dispatch => {
        return axios.post('/categories', item).then(response => {
            dispatch(fetchCategories(response.data))
        })
    }
};

export const deleteCategory = id => {
    return dispatch => {
        return axios.delete('/categories/' + id).then(response => {
            dispatch(fetchCategories(response.data));
            window.location.reload();
        })
    }
};

export const createLocation = item => {
    return dispatch => {
        return axios.post('/locations', item).then(response => {
            dispatch(fetchLocations(response.data))
        })
    }
};

export const deleteLocation = id => {
    return dispatch => {
        return axios.delete('/locations/' + id).then(response => {
            dispatch(fetchLocations(response.data));
            window.location.reload();
        })
    }
};

