import React, { Component,Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import InventorizationList from "./containers/InventorizationList/InventorizationList";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import InventForm from "./components/InventForm/InventForm";
import {Container} from "reactstrap";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar/>
            </header>
            <Container>
                <Switch>
                    <Route path="/" exact component={InventorizationList} />
                    <Route path="/add" component={InventForm} />
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
