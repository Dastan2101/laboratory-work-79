import React from 'react';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import {connect} from "react-redux";
import {createAccItem} from "../../store/actions/actions";
import Button from "@material-ui/core/Button/Button";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: theme.spacing.unit,
    },
    withoutLabel: {
        marginTop: theme.spacing.unit * 3,
    },
    textField: {
        flexBasis: 200,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});


class InventForm extends React.Component {
    state = {
        title: '',
        location_id: '',
        category_id: '',
        description: '',
        image: '',
    };

    inputChangeHandler = event => {

        this.setState({
            [event.target.name]: event.target.value

        });
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });
        this.props.createAccItem(formData);
        this.props.history.push('/')
    };

    render() {
        const {classes} = this.props;

        return (
            <div style={{width: '800px', margin: '50px auto'}}>
                <form className={classes.root} autoComplete="off">
                    <TextField
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.title}
                        name="title"
                        required
                        onChange={this.inputChangeHandler}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">Title</InputAdornment>,
                        }}
                    />

                    <TextField
                        select
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.category_id}
                        name="category_id"
                        onChange={this.inputChangeHandler}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">Categories</InputAdornment>,
                        }}
                    >
                        {this.props.categories.map(option => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.title}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        select
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.location_id}
                        name="location_id"
                        onChange={this.inputChangeHandler}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">Location</InputAdornment>,
                        }}
                    >
                        {this.props.locations.map(option => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.title}
                            </MenuItem>
                        ))}
                    </TextField>
                    <FormControl fullWidth className={classes.margin}>
                        <Input
                            value={this.state.description}
                            name="description"
                            onChange={this.inputChangeHandler}
                            startAdornment={<InputAdornment position="start">Description</InputAdornment>}
                        />
                    </FormControl>
                    <FormControl className={classes.margin}>
                        <input
                            name="image"
                            onChange={this.fileChangeHandler}
                            type="file"
                        />
                    </FormControl>

                    <Button variant="contained" color="default" className={classes.button} onClick={this.submitFormHandler}>
                        Upload
                        <CloudUploadIcon className={classes.rightIcon} />
                    </Button>

                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.inventList.categories,
    locations: state.inventList.locations
});

const mapDispatchToProps = dispatch => ({
    createAccItem: item => dispatch(createAccItem(item))
});

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(InventForm));