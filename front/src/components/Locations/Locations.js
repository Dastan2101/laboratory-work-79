import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from "@material-ui/core/Collapse/Collapse";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import AddIcon from '@material-ui/icons/Add';
import Fab from "@material-ui/core/Fab/Fab";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import Button from "@material-ui/core/Button/Button";
import purple from '@material-ui/core/colors/purple';
import Modal from "@material-ui/core/Modal/Modal";
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import TextField from "@material-ui/core/TextField/TextField";
import Input from "@material-ui/core/Input/Input";
import FormControl from "@material-ui/core/FormControl/FormControl";
import SaveIcon from '@material-ui/icons/Save';
import {createLocation, deleteLocation} from "../../store/actions/actions";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    cssRoot: {
        color: theme.palette.getContrastText(purple[500]),
        backgroundColor: purple[500],
        '&:hover': {
            backgroundColor: purple[700],
        }
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        outline: 'none',
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    }
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

class Locations extends Component {
    state = {
        openLocations: false,
        open: false,
        title: '',
        description: ''
    };

    handleClick = () => {
        this.setState(state => ({openLocations: !state.openLocations}));
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    createData = (data) => {
        if (this.state.title !== '') {
            data = {
                title: this.state.title,
                description: this.state.description !== '' ? this.state.description : null
            };
            this.props.createLocation(data);
            this.handleClose()
        }

    };


    render() {
        const {classes} = this.props;
        return (
            <Fragment>
                <List
                    component="nav"
                    className={classes.root}
                >
                    <ListItem button onClick={this.handleClick}>
                        <ListItemIcon>
                            <InboxIcon/>
                        </ListItemIcon>
                        <ListItemText inset primary="Locations"/>
                        {this.state.openLocations ? <ExpandLess/> : <ExpandMore/>}
                    </ListItem>
                    <Collapse in={this.state.openLocations} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem button className={classes.nested}>
                                <Fab size="small" color="secondary" aria-label="Add" onClick={this.handleOpen}>
                                    <AddIcon/>
                                </Fab>
                                <ListItemText inset primary="Create"/>
                            </ListItem>
                            {this.props.locations.map(location => (
                                <ListItem button className={classes.nested} key={location.id}>
                                    <IconButton aria-label="Delete" className={classes.margin}
                                                onClick={() => this.props.deleteLocation(location.id)}>
                                        <DeleteIcon fontSize="small"/>
                                    </IconButton>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className={classes.cssRoot}
                                    >
                                        {location.title}

                                    </Button>
                                </ListItem>
                            ))}
                        </List>

                    </Collapse>
                </List>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.open}
                    onClose={this.handleClose}
                >
                    <div style={getModalStyle()} className={classes.paper}>
                        <TextField
                            value={this.state.title}
                            name="title"
                            onChange={this.inputChangeHandler}
                            required
                            InputProps={{
                                startAdornment: <InputAdornment position="start">Title</InputAdornment>,
                            }}
                        />
                        <FormControl fullWidth className={classes.margin}>
                            <Input
                                value={this.state.description}
                                name="description"
                                onChange={this.inputChangeHandler}
                                startAdornment={<InputAdornment position="start">Description</InputAdornment>}
                            />
                        </FormControl>
                        <Button variant="contained" size="small" className={classes.button} onClick={this.createData}>
                            <SaveIcon className={(classes.leftIcon, classes.iconSmall)}/>
                            Save
                        </Button>
                    </div>
                </Modal>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    locations: state.inventList.locations
});

const mapDispatchToProps = dispatch => ({
    createLocation: item => dispatch(createLocation(item)),
    deleteLocation: id => dispatch(deleteLocation(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Locations));