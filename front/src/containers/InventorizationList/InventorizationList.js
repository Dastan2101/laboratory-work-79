import React, {Component} from 'react';
import {
    getCategories,
    getCategory,
    getFullInfo,
    getInventList,
    getLocation,
    getLocations, inventItem
} from "../../store/actions/actions";
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InventoryThumbNail from "../../components/InventoryThumbNail/InventoryThumbNail";
import {Col, Row} from "reactstrap";
import Categories from "../../components/Categories/Categories";
import Locations from "../../components/Locations/Locations";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton/IconButton";

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '15%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    margin: {
        margin: theme.spacing.unit,
    }
});

class InventorizationList extends Component {

    state = {
        expanded: null,
    };

    componentDidMount() {
        this.props.getLocations();
        this.props.getCategories();
        this.props.getInventList();
    }


    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    dataLoad = (id, locationId, categoryId) => {
        this.props.getFullInfo(id);
        this.props.getLocation(locationId);
        this.props.getCategory(categoryId);
    };

    render() {
        const {classes} = this.props;
        const {expanded} = this.state;
        let panel;

        if (this.props.list && this.props.locations) {
            panel = this.props.list.map(item => {
                let locId = this.props.locations[item.location_id - 1];
                let catId = this.props.categories[item.category_id - 1];

                return <ExpansionPanel expanded={expanded === item.id} onChange={this.handleChange(item.id)}
                                       key={item.id}
                                       onClick={() => this.dataLoad(item.id, locId.id, catId.id)}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                        <Typography className={classes.heading}>{item.id}</Typography>
                        <Typography className={classes.heading}>{item.title}</Typography>
                        {/*<Typography className={classes.heading}>{locId.title}</Typography>*/}
                        {/*<Typography className={classes.heading}>{catId.title}</Typography>*/}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <InventoryThumbNail image={item.image}/>
                        <div style={{display: "inline-block"}}>
                            <p className={classes.heading}
                               style={{fontWeight: 'bold'}}>
                                Inventory
                                number:
                                <span
                                    style={{color: 'red'}}>
                                    {item.id}
                                    </span>
                            </p>
                            <p className={classes.heading}
                               style={{fontWeight: 'bold'}}>
                                Description:
                                <span style={{color: 'red'}}>
                                    {this.props.fullInfo.description}
                                </span>
                            </p>
                            <p className={classes.heading}
                               style={{fontWeight: 'bold'}}>
                                Location description:
                                <span style={{color: 'red'}}>
                                    {this.props.location.description}
                                </span>
                            </p>
                            <p className={classes.heading}
                               style={{fontWeight: 'bold'}}>
                                Category description:
                                <span style={{color: 'red'}}>
                                    {this.props.category.description}
                                </span>
                            </p>
                            <p className={classes.heading}>
                                <IconButton aria-label="Delete" className={classes.margin} onClick={() => this.props.inventItem(item.id)}>
                                    <DeleteIcon fontSize="large" />
                                </IconButton>
                            </p>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

            })
        }

        return (
            <Row style={{margin: '50px auto'}}>
                <Col sm={3}>
                    <Categories/>
                    <Locations/>
                </Col>
                <Col sm={9}>
                    <div style={{width: '100%'}}>
                        <div className={classes.root}>
                            <ExpansionPanelSummary>
                                <Typography className={classes.heading}>id</Typography>
                                <Typography className={classes.heading}>Name</Typography>
                                <Typography className={classes.heading}>Locations</Typography>
                                <Typography className={classes.heading}>Categories</Typography>
                            </ExpansionPanelSummary>
                            {panel}
                        </div>
                    </div>
                </Col>
            </Row>

        );
    }
}


const mapStateToProps = state => ({
    list: state.inventList.inventList,
    fullInfo: state.inventList.fullInfo,
    locations: state.inventList.locations,
    location: state.inventList.locationInfo,
    categories: state.inventList.categories,
    category: state.inventList.category
});

const mapDispatchToProps = dispatch => ({
    getInventList: () => dispatch(getInventList()),
    getCategories: () => dispatch(getCategories()),
    getLocations: () => dispatch(getLocations()),
    getFullInfo: id => dispatch(getFullInfo(id)),
    getLocation: id => dispatch(getLocation(id)),
    getCategory: id => dispatch(getCategory(id)),
    inventItem: id => dispatch(inventItem(id))
});


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(InventorizationList));